#!/usr/bin/env python
#---------------------------------------------------------------------------
# Name:        connectpool_oradb.py
# Purpose:     Connection Pool Manager for Oracle DBMS
#              
#
# Author:      Igor Lavrinenko
#
# Created:     2015
# RCS-ID:      $Id$
# Copyright:   (c) 2015
# License:     GPL 
#---------------------------------------------------------------------------

import os
os.environ["NLS_LANG"] = "Russian.AL32UTF8"
import cx_Oracle
from connectpool import ConnectPool, ConnectPoolError

#---------------------------------------------------------------------------
class ConnectPoolOraDb(ConnectPool):

    #-----------------------------------------------------------------------    
    def connect(self):
        conn = None
        if ('host' in self.db_params) and ('port' in self.db_params) and ('sid' in self.db_params):
            tns = cx_Oracle.makedsn(self.db_params['host'], self.db_params['port'], self.db_params['sid'])
            if ('username' in self.db_params) and ('password' in self.db_params):
                conn = cx_Oracle.connect("%s/%s@%s" % (self.db_params['username'], self.db_params['password'], tns))

        if not conn:
            raise ConnectPoolError, "Incorrect parameter: 'db_params'."

        return conn


    #-----------------------------------------------------------------------    
    def disconnect(self, conn):
        conn.commit()
        conn.close()
        conn = None


#---------------------------------------------------------------------------
# oraDbPool = ConnectPoolOraDb()
