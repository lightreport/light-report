#!/usr/bin/env python
#---------------------------------------------------------------------------
# Name:        connectpool_mc.py
# Purpose:     Connection Pool Manager for memcached
#              
#
# Author:      Igor Lavrinenko
#
# Created:     2015
# RCS-ID:      $Id$
# Copyright:   (c) 2015
# License:     GPL 
#---------------------------------------------------------------------------

import memcache
from connectpool import ConnectPool

#---------------------------------------------------------------------------
class ConnectPoolMC(ConnectPool):

    #-----------------------------------------------------------------------    
    def connect(self):
        connection_info = ['127.0.0.1:11211']
        conn = memcache.Client(connection_info, debug=0)
        return conn


    #-----------------------------------------------------------------------    
    def disconnect(self, conn):
        conn = None

#---------------------------------------------------------------------------
mcPool = ConnectPoolMC()
