#!/usr/bin/env python
#---------------------------------------------------------------------------
# Name:        loginlogger.py
# Purpose:     Login logger class
#
# Author:      Igor Lavrinenko
#
# Created:     2013
# RCS-ID:      $Id$
# Copyright:   (c) 2013
# License:     GPL
#---------------------------------------------------------------------------

import time
from twisted.python import log

#---------------------------------------------------------------------------
class LoginLogger:

    #-----------------------------------------------------------------------
    def __init__(self):
        self.ClientIPs = dict()
        self._pgcec = 0


    #-----------------------------------------------------------------------
    def _perform_gc(self):
        self._pgcec += 1
        if self._pgcec > 2000:
            log.msg("PerformGC-> ")
            self._pgcec = 0 ## TODO: Delete old IPs


    #-----------------------------------------------------------------------
    def logFail(self, ipaddr, login, passwd, descr=""):
        self._perform_gc()
        if ipaddr not in self.ClientIPs:
            count = 1
        else:
            count = self.ClientIPs[ipaddr][3] + 1
        self.ClientIPs[ipaddr] = [login, passwd, int(time.time()), count]
        log.msg("LoginFail-> ip:%s cred:%s/%s cnt:%d" % (ipaddr, login, passwd, count))
        if descr: log.msg("LoginFail-> " + descr)


    #-----------------------------------------------------------------------
    def logPass(self, ipaddr, login, descr=""):
        self._perform_gc()
        self.ClientIPs[ipaddr] = [login, '***', int(time.time()), -1]
        log.msg("LoginPass-> ip:%s cred:%s/****" % (ipaddr, login))
        if descr: log.msg("LoginPass-> " + descr)


    #-----------------------------------------------------------------------
    def getRequestCount(self, ipaddr):
        if ipaddr not in self.ClientIPs:
            return 0
        else:
            return self.ClientIPs[ipaddr][3]


#---------------------------------------------------------------------------
DefaultLoginLogger = LoginLogger()
