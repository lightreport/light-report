#!/usr/bin/env python
# -*- coding: utf-8 -*-
#---------------------------------------------------------------------------
# Name:        pageelement.rpy
# Purpose:     PAGEELEMENT
#
# Author:      Igor Lavrinenko
#
# Created:     2012
# RCS-ID:      $Id$
# Copyright:   (c) 2012
# License:     GPL
#---------------------------------------------------------------------------

from twisted.web.template import Element, XMLString

#---------------------------------------------------------------------------
class PageElement(Element):

    #-----------------------------------------------------------------------
    def is_mobile_device(self):
        return self.is_mobile_phone() or self.is_mobile_tab()


    #-----------------------------------------------------------------------
    def is_mobile_phone(self):
        ret = (self.user_agent.find('Android') >= 0)
        return ret


    #-----------------------------------------------------------------------
    def is_mobile_tab(self):
        ret = (self.user_agent.find('Android') >= 0)
        return ret


    #-----------------------------------------------------------------------
    def load_js_inline(self, js_file, post_script=''):
        with open(js_file, "rb") as f: s = f.read()
        if post_script:
            s += post_script + ';'
        return XMLString('<script type="text/javascript">\n//<![CDATA[\n' + s + '\n//]]>\n</script>').load()


#---------------------------------------------------------------------------
class JsonTemplate:
    pass
