#!/usr/bin/env python
#---------------------------------------------------------------------------
# Name:        jsonext.rpy
# Purpose:     JSON extensions
#
# Author:      Igor Lavrinenko
#
# Created:     2016
# RCS-ID:      $Id$
# Copyright:   (c) 2016
# License:     GPL
#---------------------------------------------------------------------------


#---------------------------------------------------------------------------
def json_serialize_helper(obj):
    if obj is None:
        return '(null)'
    else:
        if isinstance(obj, datetime.datetime):
            ret = obj.isoformat()
            return ret
        elif isinstance(obj, decimal.Decimal):
            ret = float(obj)
            return ret
