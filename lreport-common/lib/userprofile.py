#!/usr/bin/env python
#---------------------------------------------------------------------------
# Name:        userprofile.py
# Purpose:     User profile class
#
# Author:      Igor Lavrinenko
#
# Created:     2013
# RCS-ID:      $Id$
# Copyright:   (c) 2013
# License:     GPL
#---------------------------------------------------------------------------

import hashlib, sqlite3

#---------------------------------------------------------------------------
class UserProfileError(Exception): pass

#---------------------------------------------------------------------------
class UserProfile:

    #-----------------------------------------------------------------------
    def __init__(self, user, ip='', ua=''):
        self.memlist = []
        self.memdict = {}
        self.load_user_data(user, ip, ua)


    #-----------------------------------------------------------------------
    def get_pwd_hash(self, pwd):
        return hashlib.sha1("%s_%s" % (str(self.user) , str(pwd))).hexdigest()


    #-----------------------------------------------------------------------
    def check_auth(self, entered_pwd):
        return (self.rpwd_hash == self.get_pwd_hash(entered_pwd))


    #-----------------------------------------------------------------------
    def load_user_data(self, user, ip, ua):
        self.user = user
        self.user_ip = ip
        self.user_agent = ua
        self.user_dbid = None
        self.user_name = 'anonymous'
        self.user_mail = None
        self.user_rights = []
        self.rpwd_hash = ''

        conn = sqlite3.connect('../lreport-common/data/db/profiles.db')
        try:

            cursor = conn.cursor()
            cursor.execute("SELECT user_id, user_pwd_hash, user_name, user_mail FROM users WHERE ifnull(disabled, 0) = 0 and user_login = ?", [user])
            res = cursor.fetchone()
            if res:
                self.user_dbid, self.rpwd_hash, self.user_name, self.user_mail = res
            conn.commit()

            if not res:
                raise UserProfileError, "User profile for name \"%s\" not found." % (user)

            # Load rights (only reports now)
            self.user_rights = []
            if user == 'admin':
                sql = """
                    select name, displayname, filename from reports
                """
                cursor.execute(sql)
            else:
                sql = """
                    select name, displayname, filename from reports where report_id in (
                        select obj_id from access_group where obj_type = 2 and access_group_name in (
                        select access_group_name from access_group where obj_type = 1 and obj_id = ?))
                """
                cursor.execute(sql, [self.user_dbid])

            res = cursor.fetchone()
            while res:
                self.user_rights.append(tuple(res))
                res = cursor.fetchone()
            conn.commit()

        finally:
            conn.close()

