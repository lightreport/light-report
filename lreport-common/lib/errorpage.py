#!/usr/bin/env python
# -*- coding: utf-8 -*-
#---------------------------------------------------------------------------
# Name:        errorpage
# Purpose:     ERRORPAGE
#
# Author:      Igor Lavrinenko
#
# Created:     2012
# RCS-ID:      $Id$
# Copyright:   (c) 2012
# License:     GPL
#---------------------------------------------------------------------------

import time
from twisted.web.template import XMLFile, renderer, tags
from twisted.python.filepath import FilePath
from pageelement import PageElement
from common import *

#---------------------------------------------------------------------------
class ErrorPageTemplate(PageElement):
    loader = XMLFile(FilePath(ERROR_PAGE_TEMPLATE))

    #-----------------------------------------------------------------------
    def __init__(self, msg, title=""):
        PageElement.__init__(self)
        self._msg = msg
        self._title = title


    #-----------------------------------------------------------------------
    @renderer
    def title(self, request, tag):
        return tag('Error')


    #-----------------------------------------------------------------------
    @renderer
    def error_time(self, request, tag):
        return tag(time.strftime("%Y-%m-%d %H:%M:%S"))


    #-----------------------------------------------------------------------
    @renderer
    def error_msg(self, request, tag):
        return tag(self._title, tags.br(), tags.pre(self._msg))

