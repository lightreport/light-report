#!/usr/bin/env python
#---------------------------------------------------------------------------
# Name:        logfile.py
# Purpose:     Log module (Put string messages and exceptions to log file).
#
# Author:      Igor Lavrinenko
#
# Created:     2005
# RCS-ID:      $Id: logfile.py,v 1.4 2008/06/25 09:26:29 visokey Exp $
# Copyright:   (c) 2005
# License:     GPL 
#---------------------------------------------------------------------------

import sys, traceback, threading
from time import *

#---------------------------------------------------------------------------

logfile_name  = "logfile.txt"

_logfile_lock = threading.Lock()

#---------------------------------------------------------------------------

def _log_to_file(file_name, line):
    _logfile_lock.acquire()
    try:
        f = file(file_name, "at")
        try: f.write(line + "\n") ## .encode("cp1251")
        except: pass
        f.close()
    finally:
        _logfile_lock.release()


def get_current_time():
    return strftime("%Y-%m-%d %H:%M:%S")


def add(line, echo=1):
    _log_to_file(logfile_name, "%s %s" % (get_current_time(), line))
    if echo == 1: print line


def log_exception(echo=1):
    exctype, excvalue, exctb = sys.exc_info()
    s = "".join(traceback.format_exception(exctype, excvalue, exctb))
    add(s, echo)
