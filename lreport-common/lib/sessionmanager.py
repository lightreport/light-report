#!/usr/bin/env python
#---------------------------------------------------------------------------
# Name:        sessionmanager.py
# Purpose:     Application Session Manager
#              
#
# Author:      Igor Lavrinenko
#
# Created:     2015
# RCS-ID:      $Id$
# Copyright:   (c) 2015
# License:     GPL 
#---------------------------------------------------------------------------

import sqlite3
from varcache import VarCache
## from sman_trivial import SMan_trivial
from sman_mc import SMan_mc

#---------------------------------------------------------------------------

OBSOLETE_SESSION_INTERVAL = 1795
MEMCACHE_SERVERS          = ['127.0.0.1:11211']
MEMCACHE_PREFIX           = 'lr.'

#---------------------------------------------------------------------------
class SessionManager(SMan_mc):

    _counter = 0
    sessionHistory = VarCache(20)
    sessionDB = '../lreport-common/data/db/sessions.db'

    #-----------------------------------------------------------------------    
    def loadPersistentSession(self, sessionId):
        res = None
        conn = sqlite3.connect(self.sessionDB)
        try:
            cursor = conn.cursor()
            cursor.execute("SELECT login, login_pwd_hash FROM session WHERE session_uid=? and active != 0 and datetime('now', 'localtime') between beg_time and end_time", [sessionId])
            res = cursor.fetchone()

            conn.commit()
        finally:
            conn.close()

        return res


    #-----------------------------------------------------------------------    
    def savePersistentSession(self, sessionId, login, pwd_hash, ip, ua):
        conn = sqlite3.connect(self.sessionDB)
        try:
            cursor = conn.cursor()
            try:
                cursor.execute("INSERT INTO session (session_uid, login, login_pwd_hash, user_ip, user_agent, beg_time, end_time, active) VALUES (?,?,?,?,?, datetime('now', 'localtime'), datetime('now', '+1 month', 'localtime'), 1)", [sessionId, login, pwd_hash, ip, ua])
                res = True
            except sqlite3.IntegrityError as err:
                res = False

            conn.commit()
        finally:
            conn.close()

        return res


    #-----------------------------------------------------------------------    
    def dropPersistentSession(self, sessionId):
        res = None
        conn = sqlite3.connect(self.sessionDB)
        try:
            cursor = conn.cursor()
            # cursor.execute("DELETE FROM session WHERE session_uid=? and datetime('now', 'localtime') between beg_time and end_time", [sessionId])
            cursor.execute("UPDATE session SET active = 0, end_time = datetime('now', 'localtime') WHERE session_uid=? and datetime('now', 'localtime') between beg_time and end_time", [sessionId])
            conn.commit()
        finally:
            conn.close()

        return res


    #-----------------------------------------------------------------------    
    def addSession(self, sessionId, userProfile):
        r = super(self.__class__, self).addSession(sessionId, userProfile)
        self._counter += 1
        key = 'K' + str(self._counter + 100)
        self.sessionHistory.put(key, sessionId)
        return r


    #-----------------------------------------------------------------------    
    # def getSession(self, sessionId, noTouch=False):
        # si = super(self.__class__, self).getSession(sessionId, noTouch)
        # if not si:
            # si = self.loadPermanent(sessionId)

        # self._counter += 1
        # key = 'K' + str(self._counter + 100)
        # self.sessionHistory.put(key, sessionId)
        # return r


    #-----------------------------------------------------------------------    
    def sessionIds(self):
        c = self._counter
        while True:
            key = 'K' + str(c + 100)
            val = self.sessionHistory.get(key)
            if not val: break
            c -= 1
            yield val


#---------------------------------------------------------------------------
## DefaultSessionManager = SessionManager(OBSOLETE_SESSION_INTERVAL)
DefaultSessionManager = SessionManager(MEMCACHE_SERVERS, MEMCACHE_PREFIX, OBSOLETE_SESSION_INTERVAL)
