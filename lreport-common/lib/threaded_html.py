#!/usr/bin/env python
#---------------------------------------------------------------------------
# Name:        threaded_html.py
# Purpose:     
#
# Author:      Igor Lavrinenko
#
# Created:     2012
# RCS-ID:      $Id$
# Copyright:   (c) 2012-2016
# License:     GPL
#---------------------------------------------------------------------------

import string, sys, traceback
import json as simplejson
from twisted.python import log
from twisted.internet.address import IPv4Address
from twisted.web.server import NOT_DONE_YET
from twisted.web.resource import Resource
from twisted.web.error import FlattenerError
from twisted.internet import reactor
from twisted.web.template import flattenString
from twisted.web.util import redirectTo
from errorpage import ErrorPageTemplate
from userprofile import UserProfile, UserProfileError
from sessionmanager import DefaultSessionManager

#---------------------------------------------------------------------------
PRINT_ERROR_TRACEBACK_TO_WEBUI = False
AUTH_ERROR_URL = "login"

#---------------------------------------------------------------------------
class ThreadedHtmlGenError(Exception): pass


#---------------------------------------------------------------------------
class ThreadedBaseGen(Resource):

    #-----------------------------------------------------------------------
    def __init__(self, template, no_check_auth=False, no_thread=False):
        Resource.__init__(self)
        self.login_url = AUTH_ERROR_URL
        self.no_thread = no_thread
        self.template = template
        self.template.user_agent = None
        self.template.ip_addr = None
        self.request_method = ""
        self.no_check_auth = no_check_auth
        self._error = ""
        self._error_full = PRINT_ERROR_TRACEBACK_TO_WEBUI
        self._session_info = None
        self.session_man = DefaultSessionManager
        self.default_login = None
        self.default_pwd_hash = None
        self.init()


    #-----------------------------------------------------------------------
    def init(self):
        pass ## Virtual method


    #-----------------------------------------------------------------------
    def _get_exception_text(self):
        exctype, excvalue, exctb = sys.exc_info()
        s = "".join(traceback.format_exception(exctype, excvalue, exctb))
        return s


    #-----------------------------------------------------------------------
    def _silent_auth(self, uid, login, pwd_hash, save_persist):
        try:
            up = UserProfile(login, self.template.ip_addr, self.template.user_agent)
        except UserProfileError:
            return

        if up and (up.rpwd_hash == pwd_hash):
            sess = self.session_man.addSession(uid, up)
            if save_persist:
                self.session_man.savePersistentSession(uid, login, pwd_hash, self.template.ip_addr, self.template.user_agent)
            log.msg("Silent login: %s (%s)" % (login, uid))
            return sess


    #-----------------------------------------------------------------------
    def _check_auth(self):
        si = self.get_session_info()
        if si:
            # if sess[1].user_agent == self.template.user_agent: return sess
            # else: log.msg("Bad 'user_agent' in session: " + uid)
            return si
        else:
            uid = self.session_man.getSessionId(self.req)
            if uid:
                res = self.session_man.loadPersistentSession(uid)
                if res:
                    login,pwd_hash = res
                    return self._silent_auth(uid, login, pwd_hash, False)
                else:
                    if self.default_login and self.default_pwd_hash:
                        uid = self.session_man.makeSessionId(self.req)
                        return self._silent_auth(uid, self.default_login, self.default_pwd_hash, True)
            else:
                if self.default_login and self.default_pwd_hash:
                    uid = self.session_man.makeSessionId(self.req)
                    return self._silent_auth(uid, self.default_login, self.default_pwd_hash, True)


    #-----------------------------------------------------------------------
    def get_session_info(self):
        if not self._session_info:
            self._session_info = self.session_man.getSession(self.session_man.getSessionId(self.req))
        return self._session_info


    #-----------------------------------------------------------------------
    def get_user_profile(self):
        si = self.get_session_info()
        if si:
            up = si[1]
            if up:
                return up
            else:
                raise ThreadedHtmlGenError, "Error reading UserProfile. SessionInfo = " + str(si)
        else:
            raise ThreadedHtmlGenError, "Error reading UserProfile. No SessionInfo."


    #-----------------------------------------------------------------------
    def update_user_profile(self):
        if self._session_info: self.session_man.addSession(self._session_info[0], self._session_info[1])


    #-----------------------------------------------------------------------
    def get_row_header(self, header_name):
        ret = ""
        ua = self.req.requestHeaders.getRawHeaders(header_name)
        if ua is not None and len(ua) > 0: ret = ua[0]
        return ret


    #-----------------------------------------------------------------------
    def get_arg(self, arg_name):
        res = ""
        for key, val in self.req.args.iteritems():
            if key.lower() == arg_name:
                res = string.join(val, ",")
                break
        return res


    #-----------------------------------------------------------------------
    def step_a(self):
        try:
            self.walk()
            self._render_normal_response()
        except:
            self._error = self._get_exception_text()
            self._render_error_response(self._error)
            raise


    #-----------------------------------------------------------------------
    def _flush_response(self, html):
        self.req.write("<!DOCTYPE html>\n")
        self.req.write(html)
        try:
            self.req.finish()
        except IOError as e:
            log.msg("Request.finish() I/O error({0}): {1}".format(e.errno, e.strerror))


    #-----------------------------------------------------------------------
    def _render_normal_response(self):
        flattenString(None, ErrorPageTemplate("You should not directly use the class 'ThreadedBaseGen'")).addCallback(self._flush_response)


    #-----------------------------------------------------------------------
    def _render_error_response(self, msg):
        flattenString(None, ErrorPageTemplate("You should not directly use the class 'ThreadedBaseGen'")).addCallback(self._flush_response)


    #-----------------------------------------------------------------------
    def _parse_error(self, result):
        if result.check(FlattenerError):
            msg = result.getErrorMessage()
        else:
            msg = 'Exception while flattening. Please see log for details.'
        self._render_error_response(msg)
        result.raiseException()


    #-----------------------------------------------------------------------
    def _on_result(self, success, result):
        if not success: 
            msg = str(result) if self._error_full else result.getErrorMessage()
            self._render_error_response(msg)
            result.raiseException()
        else:
            self._render_normal_response()


    #-----------------------------------------------------------------------
    def on_fail_auth(self):
        return "<html><h3>Authorization error.</h3></html>"


    #-----------------------------------------------------------------------
    # def _method_exists(self, method_name):
        # return (hasattr(self.__class__, method_name) and callable(getattr(self.__class__, method_name)))


    #-----------------------------------------------------------------------
    def _render(self, request):
        self.req = request
        self.template.user_agent = self.get_row_header('user-agent')
        realIP = self.get_row_header('X-Real-IP')
        if realIP:
            self.template.ip_addr = realIP
            log.msg("<X-Real-IP> " + realIP)
        else:
            self.template.ip_addr = request.getClientIP()
        # if realIP: request.client = IPv4Address('TCP', realIP, 80)

        if self.no_check_auth:
            self.session = ""
        else:
            self.session = self._check_auth()
            if not self.session: return self.on_fail_auth()

        if self.no_thread:
            self.step_a()
            return NOT_DONE_YET
        else:
            reactor.getThreadPool().callInThreadWithCallback(self._on_result, self.walk_in_thread)
            return NOT_DONE_YET

        # if self._method_exists('walk_in_thread'):
            # reactor.getThreadPool().callInThreadWithCallback(self._on_result, self.walk_in_thread)
            # return NOT_DONE_YET
        # elif self._method_exists('walk'):
            # self.step_a()
            # return NOT_DONE_YET
        # else:
            # return "<html><h3>Walk method not implement.</h3></html>"


    #-----------------------------------------------------------------------
    def render_GET(self, request):
        self.request_method = "GET"
        return self._render(request)


    #-----------------------------------------------------------------------
    def render_POST(self, request):
        self.request_method = "POST"
        return self._render(request)


#---------------------------------------------------------------------------
class ThreadedHtmlGen(ThreadedBaseGen):

    #-----------------------------------------------------------------------
    def _render_normal_response(self):
        flattenString(None, self.template).addCallbacks(self._flush_response, self._parse_error)


    #-----------------------------------------------------------------------
    def _render_error_response(self, msg):
        flattenString(None, ErrorPageTemplate(msg)).addCallback(self._flush_response)


    #-----------------------------------------------------------------------
    def on_fail_auth(self):
        return redirectTo(self.login_url, self.req)


#---------------------------------------------------------------------------
class ThreadedJsonGen(ThreadedBaseGen):

    #-----------------------------------------------------------------------
    def _flush_response(self, html):
        self.req.write(html)
        try:
            self.req.finish()
        except IOError as e:
            log.msg("Request.finish() I/O error({0}): {1}".format(e.errno, e.strerror))


    #-----------------------------------------------------------------------
    def _render_normal_response(self):
        self._flush_response(self.template.Response)


    #-----------------------------------------------------------------------
    def _render_error_response(self, msg):
        self._flush_response(simplejson.dumps({'e': 'Internal error (07)'}))


    #-----------------------------------------------------------------------
    def on_fail_auth(self):
        return simplejson.dumps({'e': 'Authorization ticket error (01). Please refresh page.'})

