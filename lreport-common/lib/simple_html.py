#---------------------------------------------------------------------------
class SimpleHtmlGen(Resource):

    #-----------------------------------------------------------------------
    def __init__(self, fn):
        Resource.__init__(self)
        self._fn = fn


    #-----------------------------------------------------------------------
    def step_a(self):
        self.html = self._fn()
        reactor.callFromThread(self.step_b)


    #-----------------------------------------------------------------------
    def step_b(self):
        self.req.write(self.html)
        self.req.finish()


    #-----------------------------------------------------------------------
    def render_GET(self, request):
        request.write("<!DOCTYPE html>\n")
        self.req = request
        reactor.callInThread(self.step_a)
        return NOT_DONE_YET 


#---------------------------------------------------------------------------
def threaded_renderer(fn):
    def retfn(*args):
        return SimpleHtmlGen(fn)
    return retfn

