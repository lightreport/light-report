#!/usr/bin/env python
#---------------------------------------------------------------------------
# Name:        qf2.py
# Purpose:     SQL query factory module. Generation 2.
#
# Author:      Igor Lavrinenko
#
# Created:     2015
# RCS-ID:      $Id$
# Copyright:   (c) 2015
# License:     GPL 
#---------------------------------------------------------------------------

import datetime
from collections import OrderedDict

#---------------------------------------------------------------------------

TABLE_PREFIX        = ""

FIELD_TYPE_UNKNOWN  = 0
FIELD_TYPE_STRING   = 1
FIELD_TYPE_INTEGER  = 2
FIELD_TYPE_FLOAT    = 3
FIELD_TYPE_DATETIME = 4
FIELD_TYPE_LITERAL  = 5

SQL_STYLE_GENERIC   = 0
SQL_STYLE_MYSQL     = 1
SQL_STYLE_PGSQL     = 2
SQL_STYLE_MSSQL     = 3
SQL_STYLE_ORACLE    = 4

#---------------------------------------------------------------------------
class SqlParam(object):

    #-----------------------------------------------------------------------
    def __init__(self):
        self.sql_style = SQL_STYLE_GENERIC
        self.param_no = -1


    #-----------------------------------------------------------------------
    def get_param_view(self):
        if self.sql_style == SQL_STYLE_ORACLE and self.param_no > 0:
            return ':' + str(self.param_no)
        else:
            return '?'


#---------------------------------------------------------------------------
class SqlFactory(object):

    sql_style = SQL_STYLE_GENERIC

    #-----------------------------------------------------------------------
    def __init__(self, table="", fields="", filter="", querytemplate=""):
        self.tablepref = TABLE_PREFIX
        self.tablename = table
        self.clear()
        if fields: self.fields = fields
        self.filter = filter
        self.querytemplate = querytemplate


    #-----------------------------------------------------------------------
    def __str__(self):
        return self.getquery()


    #-----------------------------------------------------------------------
    def clear(self):
        self.querytemplate = ""
        self.filter = ""
        self._orderby = []
        self.limit = None
        self._fields = OrderedDict()


    #-----------------------------------------------------------------------
    def getquery(self, *args):
        if self.querytemplate:
            result = self.querytemplate
            i = len(args)
            for arg in reversed(args):
                if type(arg) in (int, float):
                    sarg = str(arg)
                elif type(arg) is datetime.datetime:
                    sarg = arg.strftime("'%Y-%m-%d %H:%M:%S'")
                else:
                    sarg = "'%s'" % str(arg).replace("'", "''")

                result = result.replace("{VAR"+ str(i) +"}", sarg)
                i -= 1

            return result.replace("$schema.", self.tablepref)
        else:
            return self.getselectquery()


    #-----------------------------------------------------------------------
    def setfield(self, fieldname, fieldvalue, fieldtype=FIELD_TYPE_UNKNOWN):
        fieldname = fieldname.upper()
        if fieldtype == FIELD_TYPE_UNKNOWN:
            if type(fieldvalue) is int:
                fieldtype = FIELD_TYPE_INTEGER
            elif type(fieldvalue) is float:
                fieldtype = FIELD_TYPE_FLOAT
            elif type(fieldvalue) is datetime.datetime:
                fieldtype = FIELD_TYPE_DATETIME
            else:
                fieldtype = FIELD_TYPE_STRING

        self._fields[fieldname] = fieldvalue, fieldtype


    #-----------------------------------------------------------------------
    @property
    def fields(self):
        return self._fields.copy()


    #-----------------------------------------------------------------------
    @fields.setter
    def fields(self, value):
        if type(value) in (list, tuple):
            for v in value:
                if type(v) in (list, tuple):
                    l = len(v)
                    fn = v[0]
                    fv = v[1] if l > 1 else None
                    ft = v[2] if l > 2 else FIELD_TYPE_UNKNOWN
                    self.setfield(fn, fv, ft)
                elif type(v) is str:
                    self.setfield(v, None, FIELD_TYPE_UNKNOWN)
                else:
                    raise AttributeError("Incorrect field definition")
        elif type(value) is str:
            self.setfield(value, None, FIELD_TYPE_UNKNOWN)
        else:
            raise AttributeError("Unsupported argument type")

        
    #-----------------------------------------------------------------------
    def ismodified(self):
        return self._fields


    #-----------------------------------------------------------------------
    def fmtfieldvalue(self, field):
        res = None
        if len(field) == 2:
            fv, ft = field
            if ft == FIELD_TYPE_STRING:
                if fv is not None:
                    if isinstance(fv, (unicode, str)): 
                        preres = fv
                    else:    
                        preres = str(fv)
                    res = "'%s'" % preres.replace("'", "''")

            elif ft in (FIELD_TYPE_INTEGER, FIELD_TYPE_FLOAT, FIELD_TYPE_LITERAL):
                res = str(fv)

            elif ft == FIELD_TYPE_DATETIME:
                res = fv.strftime("'%Y-%m-%d %H:%M:%S'")

        return res


    #-----------------------------------------------------------------------
    def setfilter(self, template, *args):
        nargs = []
        for arg in args:
            if (type(arg) is int) or (type(arg) is float):
                narg = arg
            elif isinstance(arg, SqlFactory):
                narg = arg.getquery()
            else:
                narg = "'%s'" % str(arg).replace("'", "''").replace(",", "','")

            nargs.append(narg)

        self.filter = template % tuple(nargs)


    #-----------------------------------------------------------------------
    def addorderby(self, fieldname):
        self._orderby.append(fieldname)


    #-----------------------------------------------------------------------
    def getselectquery(self, clausewhere=""):
        if self.querytemplate: return self.getquery()
        if self.tablename:
            fds = ','.join(self._fields)
            res = "select %s from %s%s" % (fds if fds else '*', self.tablepref, self.tablename)
            w = clausewhere if clausewhere.strip() else self.filter
            res += " where " + w if w else ""
            if self._orderby: res += " order by " + ','.join(self._orderby)
            if self.limit: res += " limit " + str(self.limit)
            return res


    #-----------------------------------------------------------------------
    def getinsertquery(self, replace_cmd=False):
        if self.tablename and self._fields:
            flds = []
            vals = []
            param_no = 0
            for k,v in self._fields.iteritems():
                fv,ft = v
                if fv is not None:
                    flds.append(k)
                    if isinstance(fv, SqlParam):
                        fv.sql_style = self.sql_style
                        param_no += 1
                        fv.param_no = param_no
                        vals.append(fv.get_param_view())
                    else:
                        vals.append(self.fmtfieldvalue(v))
            if flds: return "%s into %s%s (%s) values (%s)" % ("replace" if replace_cmd else "insert", self.tablepref, self.tablename, ','.join(flds), ','.join(vals))


    #-----------------------------------------------------------------------
    def getupdatequery(self, clausewhere=""):
        if self.tablename and self._fields:
            flds = []
            param_no = 0
            for k,v in self._fields.iteritems():
                fv,ft = v
                if isinstance(fv, SqlParam):
                    fv.sql_style = self.sql_style
                    param_no += 1
                    fv.param_no = param_no
                    flds.append(k +'='+ fv.get_param_view())
                else:
                    flds.append(k +'='+ (self.fmtfieldvalue(v) if fv is not None else 'NULL'))
                    
            where = clausewhere if clausewhere.strip() else self.filter
            if not where: raise AttributeError("Clause WHERE can't define")
            return "update %s%s set %s where %s" % (self.tablepref, self.tablename, ','.join(flds), where)


    #-----------------------------------------------------------------------
    def getdeletequery(self, clausewhere=""):
        if self.tablename:    
            where = clausewhere if clausewhere.strip() else self.filter
            if not where: raise AttributeError("Clause WHERE can't define")
            return "delete from %s%s where %s" % (self.tablepref, self.tablename, where)


#---------------------------------------------------------------------------
class InsertSqlFactory(SqlFactory):

    #-----------------------------------------------------------------------
    def __init__(self, table, fields):
        super(self.__class__, self).__init__(table, fields)


    #-----------------------------------------------------------------------
    def __str__(self):
        return self.getinsertquery()


#---------------------------------------------------------------------------
class UpdateSqlFactory(SqlFactory):

    #-----------------------------------------------------------------------
    def __init__(self, table, fields, filter):
        super(self.__class__, self).__init__(table, fields, filter)


    #-----------------------------------------------------------------------
    def __str__(self):
        return self.getupdatequery()
