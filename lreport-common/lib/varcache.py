#!/usr/bin/env python
#---------------------------------------------------------------------------
# Name:        varcache.py
# Purpose:     Variable cache
#
# Author:      Igor Lavrinenko
#
# Created:     2015
# RCS-ID:      $Id$
# Copyright:   (c) 2015
# License:     GPL
#---------------------------------------------------------------------------

import threading

#---------------------------------------------------------------------------
class VarCache(object):

    #-----------------------------------------------------------------------
    def __init__(self, cache_size=1000):
        self._stor = dict()
        self._stor_keys = []
        self._cache_size = cache_size
        self._stor_lock = threading.RLock()


    #-----------------------------------------------------------------------
    def get(self, key):
        with self._stor_lock:
            res = self._stor[key] if key in self._stor else None 
        if not res:
            res = self.get_persistent(key)
            if res: self.put(key, res, False)
        return res


    #-----------------------------------------------------------------------
    def put(self, key, obj, save_persistent=True):
        with self._stor_lock:
            while len(self._stor) >= self._cache_size or len(self._stor_keys) >= self._cache_size:
                if self._stor_keys[0] in self._stor:
                    del self._stor[self._stor_keys[0]]
                del self._stor_keys[0]

            if key not in self._stor:
                self._stor_keys.append(key)
            self._stor[key] = obj

        if save_persistent: self.put_persistent(key, obj)


    #-----------------------------------------------------------------------
    def get_persistent(self, key):
        pass


    #-----------------------------------------------------------------------
    def put_persistent(self, key, obj):
        pass

