#!/usr/bin/env python
#---------------------------------------------------------------------------
# Name:        lightquery.py
# Purpose:     Light Query
#
# Author:      Igor Lavrinenko
#
# Created:     2016
# RCS-ID:      $Id$
# Copyright:   (c) 2016
# License:     GPL
#---------------------------------------------------------------------------

from connectpool import ConnectionPoolList, PConnect

#---------------------------------------------------------------------------
class LightQueryBatch(object):

    #-----------------------------------------------------------------------
    def __init__(self):
        self.queries = []


#---------------------------------------------------------------------------
class LightQueryBase(object):

    #-----------------------------------------------------------------------
    def __init__(self, conn_pool):
        self.RaiseIntegrityErrors = True
        self.MaxResultRowset = 3000
        self._init_exec_sql()
        self.conn_pool = conn_pool


    #-----------------------------------------------------------------------
    def __del__(self):
        ## print 'LightQuery: Cleanup.'
        pass

    #-----------------------------------------------------------------------
    def _init_exec_sql(self):
        self.ErrorCode = 0
        self.ErrorMessage = u''

    #-----------------------------------------------------------------------
    def exec_sql(self, sql, sqlparams=()):
        pass

    #-----------------------------------------------------------------------
    def get_rdata(self, sql, sqlparams=()):
        pass


#---------------------------------------------------------------------------
class LightQueryOradb(LightQueryBase):

    #-----------------------------------------------------------------------
    def exec_sql(self, sql, sqlparams=()):
        import cx_Oracle
        res = None
        self._init_exec_sql()
        with PConnect(self.conn_pool) as ora:
            cursor = ora.cursor()
            try:

                if isinstance(sql, LightQueryBatch):
                    res = []
                    for s, p in sql.queries:
                        if p:
                            cursor.execute(s, p)
                        else:
                            cursor.execute(s)
                        res.append(cursor.rowcount)
                else:
                    cursor.execute(sql, sqlparams)
                    res = cursor.rowcount

            except cx_Oracle.IntegrityError as e:
                error, = e.args
                self.ErrorCode = error.code
                self.ErrorMessage = error.message
                if self.RaiseIntegrityErrors: 
                    ora.rollback()
                    raise
            except:
                ora.rollback()
                raise
            ora.commit()
            return res


    #-----------------------------------------------------------------------
    def get_rdata(self, sql, sqlparams=()):
        self._init_exec_sql()
        with PConnect(self.conn_pool) as ora:
            cursor = ora.cursor()
            cursor.execute(sql, sqlparams)
            try:

                res = []
                rowCount = 0
                while True:
                    data = cursor.fetchone()
                    if not data: break
                    res.append(list(data))
                    rowCount += 1
                    if rowCount >= self.MaxResultRowset: break

            finally:
                cursor.close()
                ora.commit()
            return res


#---------------------------------------------------------------------------
class LightQueryFactory(object):

    #-----------------------------------------------------------------------
    def __init__(self, conn_params):
        self.conn_params = conn_params


    #-----------------------------------------------------------------------
    def lq(self):
        ct = self.conn_params.connection_type.lower()
        if ct == 'oracle':
            key = ct + '_' + str(self.conn_params.connection_pool_id)
            if key in ConnectionPoolList:
                res = LightQueryOradb(ConnectionPoolList[key])
            else:
                from connectpool_oradb import ConnectPoolOraDb

                if not self.conn_params.params:
                    raise ConnectPoolError, "Incorrect connection parameters."

                oraDbPool = ConnectPoolOraDb()
                oraDbPool.db_params.update(self.conn_params.params)
                ConnectionPoolList[key] = oraDbPool
                res = LightQueryOradb(oraDbPool)
            return res

