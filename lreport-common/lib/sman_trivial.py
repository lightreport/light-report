#!/usr/bin/env python
#---------------------------------------------------------------------------
# Name:        sman_trivial.py
# Purpose:     Core Session Manager (trivial)
#              
#
# Author:      Igor Lavrinenko
#
# Created:     2008
# RCS-ID:      $Id$
# Copyright:   (c) 2008, 2011, 2015
# License:     GPL 
#---------------------------------------------------------------------------

from time import time
from uuid import uuid1
from twisted.internet import reactor
from twisted.python import log


#---------------------------------------------------------------------------
class SMan_trivial(object):

    _Sessions = []

    #-----------------------------------------------------------------------    
    def __init__(self, obsolete_session_interval=1795):
        self.obsolete_session_interval = obsolete_session_interval
        self.cookiename = b'TWISTED_SESSION'
        self._gc_starter()


    #-----------------------------------------------------------------------
    def _gc_starter(self):
        try:
            self.garbageCollection()
        finally:
            reactor.callLater(60, self._gc_starter)


    #-----------------------------------------------------------------------    
    def makeSessionId(self, request):
        r = uuid1().urn[9:].replace('-', '')
        request.addCookie(self.cookiename, r, path=b'/')
        return r


    #-----------------------------------------------------------------------    
    def getSessionId(self, request):
        sessionCookie = request.getCookie(self.cookiename)
        if sessionCookie:
            return sessionCookie


    #-----------------------------------------------------------------------    
    def addSession(self, sessionId, userProfile):
        sinfo = [sessionId, userProfile, int(round(time()))]
        self._Sessions.append(sinfo)
        log.msg('<addSession> ' + sessionId)
        return sinfo[0]
    
    
    #-----------------------------------------------------------------------    
    def getSession(self, sessionId, noTouch=False):
        for i in range(len(self._Sessions)):
            if self._Sessions[i][0] == sessionId:
                if not noTouch: self._Sessions[i][2] = int(round(time()))
                return self._Sessions[i]


    #-----------------------------------------------------------------------    
    def delSession(self, sessionId):
        res = 0
        for i in range(len(self._Sessions)):
            if self._Sessions[i][0] == sessionId:
                del self._Sessions[i]
                res = 1
                break
                
        return res


    #-----------------------------------------------------------------------    
    def touchSession(self, sessionId):
        res = 0
        for i in range(len(self._Sessions)):
            if self._Sessions[i][0] == sessionId:
                self._Sessions[i][2] = int(round(time()))
                res = 1
                break
                
        return res


    #-----------------------------------------------------------------------    
    def garbageCollection(self):
        curr_t = int(round(time()))
        i = 0
        while 1:
            if i == len(self._Sessions): break
            sess, user, t = self._Sessions[i]
            if (t + self.obsolete_session_interval) < curr_t:
                del self._Sessions[i]
                log.msg('<removeSession> ' + sess)
            else:
                i += 1                

