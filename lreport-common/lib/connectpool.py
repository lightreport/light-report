#!/usr/bin/env python
#---------------------------------------------------------------------------
# Name:        connectpool.py
# Purpose:     Connection Pool Manager
#              
#
# Author:      Igor Lavrinenko
#
# Created:     2015
# RCS-ID:      $Id$
# Copyright:   (c) 2015
# License:     GPL 
#---------------------------------------------------------------------------

import threading, datetime, time
from twisted.internet import reactor
from twisted.python import log

#---------------------------------------------------------------------------
class ConnectPoolError(Exception): pass


#---------------------------------------------------------------------------
class ConnectPoolParams(object):

    #-----------------------------------------------------------------------    
    def __init__(self, connection_type='Unknown', connection_pool_id=1):
        self.params = {}
        self.connection_type = connection_type
        self.connection_pool_id = connection_pool_id


#---------------------------------------------------------------------------
class ConnectPool(object):

    #-----------------------------------------------------------------------    
    def __init__(self):
        self.db_params = {}
        self._items = []
        self._lock = threading.RLock()
        self._maxconn = 5
        self._debug = 1
        self._gc_starter()


    #-----------------------------------------------------------------------    
    def __del__(self):
        for c in self._items:
            self.disconnect(c['pitem'])


    #-----------------------------------------------------------------------    
    def connect(self):
        pass


    #-----------------------------------------------------------------------    
    def disconnect(self, conn):
        conn = None


    #-----------------------------------------------------------------------    
    def disconnect_all(self):
        with self._lock:
            for item in self._items:
                self.disconnect(item['pitem'])
                self._items.remove(item)


    #-----------------------------------------------------------------------    
    def get(self):
        conn = None
        while not conn:
            with self._lock:
                freeItems = filter(lambda x: x['state'] == 0, self._items)
                now = datetime.datetime.now()
                if not freeItems:
                    if len(self._items) >= self._maxconn:
                        time.sleep(0.01)
                        continue
                    conn = self.connect()
                    item = {'pitem': conn, 'state': 1, 'stime': now}
                    self._items.append(item)
                    if self._debug > 0:
                        print('Pool items ----- ', self)
                        print(self._items)
                else:
                    item = freeItems[0]
                    item['state'] = 1
                    item['ltime'] = now
                    conn = item['pitem']
        return conn


    #-----------------------------------------------------------------------    
    def put(self, conn):
        with self._lock:
            for item in self._items:
                if item['pitem'] == conn: 
                    item['state'] = 0
                    item['stime'] = datetime.datetime.now()


    #-----------------------------------------------------------------------
    def _gc_starter(self):
        try:
            self.garbageCollection()
        finally:
            reactor.callLater(60, self._gc_starter)


    #-----------------------------------------------------------------------    
    def garbageCollection(self):
        now = datetime.datetime.now()
        with self._lock:
            for item in self._items:
                if (item['state'] == 0) and ((item['stime'] + datetime.timedelta(seconds=360)) < now):
                    self.disconnect(item['pitem'])
                    self._items.remove(item)
                    if self._debug > 0: log.msg('<autodisconnect>: Len of pool_items = ' + str(len(self._items)))


#---------------------------------------------------------------------------
class PConnect(object):

    #-----------------------------------------------------------------------    
    def __init__(self, connectPool):
        self.connect = None
        self.connectPool = connectPool


    #-----------------------------------------------------------------------    
    def __enter__(self):
        self.connect = self.connectPool.get()
        return self.connect


    #-----------------------------------------------------------------------    
    def __exit__(self, type, value, traceback):
        self.connectPool.put(self.connect)
        self.connect = None


#---------------------------------------------------------------------------
ConnectionPoolList = {}
