#!/usr/bin/env python
#---------------------------------------------------------------------------
# Name:        sman_mc.py
# Purpose:     Core Session Manager (memcached)
#              
#
# Author:      Igor Lavrinenko
#
# Created:     2015
# RCS-ID:      $Id$
# Copyright:   (c) 2015
# License:     GPL 
#---------------------------------------------------------------------------

from time import time
from uuid import uuid1
from twisted.python import log
from connectpool import PConnect
from connectpool_mc import mcPool

#---------------------------------------------------------------------------
class SMan_mc(object):

    _Sessions = []

    #-----------------------------------------------------------------------    
    def __init__(self, mc_server, mc_prefix, obsolete_session_interval=1795):
        self.obsolete_session_interval = obsolete_session_interval
        self.mc_prefix = mc_prefix
        self.cookiename = b'TWISTED_SESSION'


    #-----------------------------------------------------------------------    
    def makeSessionId(self, request):
        r = uuid1().urn[9:].replace('-', '')
        request.addCookie(self.cookiename, r, path=b'/')
        return r


    #-----------------------------------------------------------------------    
    def getSessionId(self, request):
        sessionCookie = request.getCookie(self.cookiename)
        if sessionCookie:
            return sessionCookie


    #-----------------------------------------------------------------------    
    def addSession(self, sessionId, userProfile):
        if not sessionId: return
        with PConnect(mcPool) as mc:
            si = [sessionId, userProfile, int(round(time()))]
            mc.set(self.mc_prefix + sessionId, si, self.obsolete_session_interval)
            return si
    
    
    #-----------------------------------------------------------------------    
    def getSession(self, sessionId, noTouch=False):
        if not sessionId: return
        with PConnect(mcPool) as mc:
            si = mc.get(self.mc_prefix + sessionId)
            if si:
                if not noTouch: 
                    si[2] = int(round(time()))
                    ## TODO: Replace to 'touch' method (+)
                    mc.set(self.mc_prefix + sessionId, si, self.obsolete_session_interval)
                return si


    #-----------------------------------------------------------------------    
    def delSession(self, sessionId):
        r = 0
        if not sessionId: return
        with PConnect(mcPool) as mc:
            mc.delete(self.mc_prefix + sessionId)
            r = 1
        return r


    #-----------------------------------------------------------------------    
    def touchSession(self, sessionId):
        r = 0
        if not sessionId: return
        with PConnect(mcPool) as mc:
            si = self._mc.get(self.mc_prefix + sessionId)
            si[2] = int(round(time()))
            mc.touch(self.mc_prefix + sessionId, si, self.obsolete_session_interval)
            r = 1
        return r


    #-----------------------------------------------------------------------    
    def garbageCollection(self):
        pass


    #-----------------------------------------------------------------------    
    def mc_getSlabs(self):
        with PConnect(mcPool) as mc:
            slabs = mc.get_slabs()
            return slabs


    #-----------------------------------------------------------------------    
    def mc_getStats(self, args):
        with PConnect(mcPool) as mc:
            slabs = mc.get_stats(args)
            return slabs

