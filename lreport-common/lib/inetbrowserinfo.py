#!/usr/bin/env python
#---------------------------------------------------------------------------
# Name:        inetbrowserinfo.py
# Purpose:     Internet Browser Info
#
# Author:      Igor Lavrinenko
#
# Created:     2016
# RCS-ID:      $Id$
# Copyright:   (c) 2016
# License:     GPL
#---------------------------------------------------------------------------

import os

#---------------------------------------------------------------------------
class InetBrowserInfo(object):

    _browsers_mask = ('chrome', 'firefox', 'msie', 'trident')
    _browsers = ('Chrome', 'Firefox', 'MSIE', 'MSIE')

    #-----------------------------------------------------------------------
    def __init__(self, user_agent):
        self.UserAgentString = user_agent
        self._ua_str_lower = user_agent.lower()
        self._parse_ua()


    #-----------------------------------------------------------------------
    def _parse_ua(self):
        browser = 'Unknown'
        version = 'Unknown'
        for i, bm in enumerate(self._browsers_mask):
            ps = self._ua_str_lower.find(bm)
            if ps >= 0:
                browser = self._browsers[i]
                if bm == 'msie':
                    l = self.UserAgentString[ps+5:].replace(';', ')', 1).split(')')
                    version = l[0]
                elif bm == 'trident':
                    l = self.UserAgentString[ps+8:].replace(';', ')', 1).split(')')
                    tver = l[0]
                else:
                    l = self.UserAgentString[ps:].replace('/', ' ', 1).split(' ')
                    version = l[1] if len(l) > 0 else version
                break
        self._browser_name = browser
        self._browser_version = version


    #-----------------------------------------------------------------------
    @property
    def name(self):
        return self._browser_name
 
 
    #-----------------------------------------------------------------------
    @property
    def version(self):
        return self._browser_version


    #-----------------------------------------------------------------------
    @property
    def version_major(self):
        try:
            return int(self._browser_version.split('.')[0])
        except ValueError:
            return None
