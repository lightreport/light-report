#!/usr/bin/env python
#---------------------------------------------------------------------------
# Name:        myconfig.py
# Purpose:     
#
# Author:      Igor Lavrinenko
#
# Created:     2013
# RCS-ID:      $Id$
# Copyright:   (c) 2013-2016
# License:     GPLv3
#---------------------------------------------------------------------------

import os, sys, yaml
from collections import namedtuple

#---------------------------------------------------------------------------
class MyConfig(object):

    #-----------------------------------------------------------------------
    def __init__(self, cfg_file=None):
        self.StrongOptions = False
        self.config_file_opt = None
        self.opt = {}

        if cfg_file:
            self.cfg_file = cfg_file
        else:
            self.cfg_file = self._get_default_cfg_file()
        self.read_config_file()
        self.check_options(self.config_file_opt, self.opt)


    #-----------------------------------------------------------------------
    def __getattr__(self, key):
        if key in self.opt:
            return self.opt[key]
        else:    
            return getattr(self.config_file_opt, key) if self.StrongOptions else getattr(self.config_file_opt, key, None)


    #-----------------------------------------------------------------------
    def _get_default_cfg_file(self):
        if hasattr(sys, "frozen"):
            b = os.path.splitext(sys.executable)
        else: 
            b = os.path.splitext(sys.argv[0])
        return b[0] + '.cfg'


    #-----------------------------------------------------------------------
    def read_config_file(self):
        opt = yaml.load(file(self.cfg_file, 'rb').read())
        Opt = namedtuple('YamlConfigFile', ' '.join(opt.keys()))
        self.config_file_opt = Opt(**opt)


    #-----------------------------------------------------------------------
    def check_options(self, config_file_opt, in_memory_opt):
        pass

