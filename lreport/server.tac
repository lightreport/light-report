from twisted.application import internet, service
from twisted.web import static, server, script
from twisted.internet import reactor
from twisted.python.log import ILogObserver, FileLogObserver
from twisted.python.logfile import DailyLogFile
from common import *

import sys
sys.path.append(BASE_DIR + "lib")
sys.path.append(LIB_DIR + "lib")

import threaded_html, qf2
qf2.TABLE_PREFIX = "ccapps_"

# --- Instance settings
LISTEN_PORT = 9090
LOG_NAME    = 'lr0.log'
# ----------

root = static.File(BASE_DIR + "root")

app_dir = static.File(BASE_DIR + "app")
app_dir.ignoreExt(".rpy")
app_dir.processors = {'.rpy': script.ResourceScript}
root.putChild(WEBAPP_ROOT, app_dir)

# -- Uncomment for use without NGINX
app_dir.putChild('js', static.File(pathJoin(pathJoin(BASE_DIR, "static"), "js")))
app_dir.putChild('img', static.File(pathJoin(pathJoin(BASE_DIR, "static"), "img")))
app_dir.putChild('fonts', static.File(pathJoin(pathJoin(BASE_DIR, "static"), "fonts")))
app_dir.putChild('css', static.File(pathJoin(pathJoin(BASE_DIR, "static"), "css")))
app_dir.putChild('cache', static.File(pathJoin(pathJoin(BASE_DIR, "static"), "cache")))

application = service.Application('web')
logfile = DailyLogFile(LOG_NAME, LOG_DIR)
application.setComponent(ILogObserver, FileLogObserver(logfile).emit)
sc = service.IServiceCollection(application)
site = server.Site(root)
i = internet.TCPServer(LISTEN_PORT, site)
i.setServiceParent(sc)

Vartainer.connection_pool_list = {}
threaded_html.PRINT_ERROR_TRACEBACK_TO_WEBUI = PRINT_ERROR_TRACEBACK_TO_WEBUI

def _drop_pools():
    # oraDbPool.disconnect_all()
    # oraDbPool = None
    print('_drop_pools...')

reactor.addSystemEventTrigger('before', 'shutdown', _drop_pools)
