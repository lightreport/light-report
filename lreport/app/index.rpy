#!/usr/bin/env python
#---------------------------------------------------------------------------
# Name:        index.rpy
# Purpose:     INDEX
#
# Author:      Igor Lavrinenko
#
# Created:     2014
# RCS-ID:      $Id$
# Copyright:   (c) 2014
# License:     GPL
#---------------------------------------------------------------------------

from twisted.web.template import XMLFile, renderer
from twisted.python.filepath import FilePath
from threaded_html import ThreadedHtmlGen
from pageelement import PageElement
from common import *

#---------------------------------------------------------------------------
class IndexTemplate(PageElement):
    loader = XMLFile(FilePath(INDEX_TEMPLATE))

    #-----------------------------------------------------------------------
    @renderer
    def meta_viewport(self, request, tag):
        if self.is_mobile_device():
            return tag("")
        else:
            return tag.clear()


    #-----------------------------------------------------------------------
    @renderer
    def title(self, request, tag):
        return tag("Redirect page")


    #-----------------------------------------------------------------------
    @renderer
    def redirect_url(self, request, tag):
        return tag('URL', href=self.redirectUrl)


    #-----------------------------------------------------------------------
    @renderer
    def footer(self, request, tag):
        return tag("Thank you for using " + WEBAPP_NAME)


#---------------------------------------------------------------------------
class IndexHtmlGen(ThreadedHtmlGen):

    #-----------------------------------------------------------------------
    def walk_in_thread(self):
        self.template.Header = ''
        self.template.redirectUrl = 'light-report'
        self.req.redirect(self.template.redirectUrl)


#---------------------------------------------------------------------------
resource = IndexHtmlGen(IndexTemplate(), True)
