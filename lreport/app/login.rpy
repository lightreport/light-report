#!/usr/bin/env python
#---------------------------------------------------------------------------
# Name:        login.rpy
# Purpose:     Login prompt
#
# Author:      Igor Lavrinenko
#
# Created:     2012
# RCS-ID:      $Id$
# Copyright:   (c) 2012-2014
# License:     GPL
#---------------------------------------------------------------------------

import os
from twisted.web.template import Element, XMLFile, XMLString, renderer, tags
from twisted.python.filepath import FilePath
from twisted.web.util import redirectTo
from twisted.python import log
from threaded_html import ThreadedHtmlGen
from pageelement import PageElement
from sessionmanager import DefaultSessionManager
from loginlogger import DefaultLoginLogger
from userprofile import UserProfile, UserProfileError
from common import *

#---------------------------------------------------------------------------
class LoginPage(PageElement):
    loader = XMLFile(FilePath(LOGIN_TEMPLATE))
    # extraContent = XMLString("""<div id="abc_id"/>""")

    #-----------------------------------------------------------------------
    @renderer
    def app_name(self, request, tag):
        return tag(WEBAPP_NAME)


    #-----------------------------------------------------------------------
    @renderer
    def footer(self, request, tag):
        return tag(tags.b(self.footermsg))


    # @renderer
    # def extrat_content(self, request, tag):
        # return self.extraContent.load()


#---------------------------------------------------------------------------
class LoginHtmlGen(ThreadedHtmlGen):

    #-----------------------------------------------------------------------
    def walk_in_thread(self):
        ip = self.template.ip_addr
        sm = DefaultSessionManager
        self.template.msg = "Log in"
        self.template.footermsg = ""
        self.template.use_captcha = (DefaultLoginLogger.getRequestCount(ip) > 2)
        login = self.get_arg("inputuser")
        password = self.get_arg("inputpassword")
        rememberMe = self.get_arg("rememberme")
        action = self.get_arg("action")
        if action.lower() == "logout": 
            uid = sm.getSessionId(self.req)
            if uid:
                sm.dropPersistentSession(uid)
                sm.delSession(uid)
                redirectTo('/' + WEBAPP_ROOT + '/login', self.req)
                log.msg('Logout-> ' + uid)

        if self.request_method == "POST":
            try:
                up = UserProfile(login, ip, self.template.user_agent)
                pwd_hash = up.get_pwd_hash(password)
                if up.rpwd_hash == pwd_hash:
                    uid = sm.getSessionId(self.req)
                    if not uid:
                        uid = sm.makeSessionId(self.req)
                    DefaultLoginLogger.logPass(ip, login, "SessionID: " + uid)
                    sm.addSession(uid, up)
                    if rememberMe: 
                        sm.savePersistentSession(uid, login, pwd_hash, ip, self.template.user_agent)
                    redirectTo('/' + WEBAPP_ROOT, self.req)
                    # if not sess: self.req.redirect(WEBAPP_ROOT) # Alternative redirect
                else:
                    DefaultLoginLogger.logFail(ip, login, password)
                    self.template.footermsg = "Please enter a correct username and password. Note that both fields are case-sensitive."
            except UserProfileError:
                DefaultLoginLogger.logFail(ip, login, password)
                self.template.footermsg = "Please enter a correct username and password. Note that both fields are case-sensitive."


#---------------------------------------------------------------------------
resource = LoginHtmlGen(LoginPage(), True)
