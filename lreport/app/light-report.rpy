#!/usr/bin/env python
#---------------------------------------------------------------------------
# Name:        light-report.rpy
# Purpose:     Light reporting module
#
# Author:      Igor Lavrinenko
#
# Created:     2015
# RCS-ID:      $Id$
# Copyright:   (c) 2015
# License:     GPL
#---------------------------------------------------------------------------

import hashlib
from twisted.web.template import XMLFile, XMLString, renderer, tags
from twisted.python.filepath import FilePath
from twisted.python import log
from threaded_html import ThreadedHtmlGen
from pageelement import PageElement
from inetbrowserinfo import InetBrowserInfo
from common import *


#---------------------------------------------------------------------------
class ReportLightTpl(PageElement):
    loader = XMLFile(FilePath(REPORT_LIGHT_TPL))

    #-----------------------------------------------------------------------
    @renderer
    def meta_ie(self, request, tag):
        bi = InetBrowserInfo(self.user_agent)
        if bi.name == 'MSIE':
            return tag("")
        else:
            return tag.clear()


    #-----------------------------------------------------------------------
    @renderer
    def lreport_id(self, request, tag):
        return tag('', value=str(self.lr_id))


    #-----------------------------------------------------------------------
    @renderer
    def css_libs(self, request, tag):
        css = (CSS_BOOTSTRAP, CSS_BOOTSTRAP_DATETIMEPICKER, CSS_BOOTSTRAP_DIALOG, CSS_STARTER)
        return tag(map(lambda x: tags.link(href=x, rel="stylesheet"), css))


    #-----------------------------------------------------------------------
    @renderer
    def js_libs(self, request, tag):
        jsl = (JS_JQUERY, JS_BOOTSTRAP, JS_UNDERSCORE, JS_MOMENTS, JS_BOOTSTRAP_DATETIMEPICKER, JS_BOOTSTRAP_DIALOG, JS_CLASS_EXTEND, JS_LRTOOLS)
        return tag(map(lambda x: tags.script(src=x, type="text/javascript"), jsl))


    #-----------------------------------------------------------------------
    @renderer
    def js_inline(self, request, tag):
        # with open(REPORT_LIGHT_JST, "rb") as f: s = f.read()
        # s = minify(s, mangle=True, mangle_toplevel=True)
        # xml = XMLString('<script type="text/javascript">\n//<![CDATA[\n' + s + '\n//]]>\n</script>')
        # return xml.load()
        if self.user_profile.user_mail:
            um = self.user_profile.user_mail.lower()
        else:
            um = 'fake@example.com'
        ps = "var user_profile_avatar = 'https://www.gravatar.com/avatar/%s?s=40&d=mm'" % hashlib.md5(um).hexdigest()
        return self.load_js_inline(REPORT_LIGHT_JST, ps)


    #-----------------------------------------------------------------------
    @renderer
    def menu_home(self, request, tag):
        return tag('Home', href='/' + WEBAPP_ROOT)


    #-----------------------------------------------------------------------
    @renderer
    def avatar_img(self, request, tag):
        return tag(src="img/avatar-default-icon2.png")


    #-----------------------------------------------------------------------
    @renderer
    def user_name(self, request, tag):
        return tag(self.user_profile.user_name)


    #-----------------------------------------------------------------------
    @renderer
    def report_items(self, request, tag):
        for n, dn, fn in self.user_profile.user_rights:
            yield tag.clone().fillSlots(rep_data_lrid=n, rep_dispname=dn)


#---------------------------------------------------------------------------
class ReportLightGen(ThreadedHtmlGen):

    #-----------------------------------------------------------------------
    def walk_in_thread(self):
        # self.Query = getArgByName(self.req.args.items(), "lr-id")
        # self.template.Header = self.Query
        self.template.lr_id = ''
        self.template.user_profile = self.get_user_profile()


#---------------------------------------------------------------------------
resource = ReportLightGen(ReportLightTpl())
