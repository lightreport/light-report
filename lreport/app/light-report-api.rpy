#!/usr/bin/env python
#---------------------------------------------------------------------------
# Name:        light-report-api.rpy
# Purpose:     Light Report JSON web api
#
# Author:      Igor Lavrinenko
#
# Created:     2015
# RCS-ID:      $Id$
# Copyright:   (c) 2015
# License:     GPL
#---------------------------------------------------------------------------

import os, json, time, datetime, string, random, xlwt
from twisted.python import log
from threaded_html import ThreadedJsonGen
from pageelement import JsonTemplate
from lightquery import LightQueryFactory
from lightreportlib import json_serialize_helper, LRMetadata
from common import *

#---------------------------------------------------------------------------
def sec2hms(sec):
    try:
        s = int(sec)
        h = int(s/3600)
        s = s - (h*3600)
        m = int(s/60)
        s = s - (m*60)
        return '%.2d:%.2d:%.2d' % (h,m,s)
    except:
        return sec


def gdmdate(s):
    try:
        return s[6:8] + '.' + s[4:6] + '.' + s[0:4]
    except:
        return s


#---------------------------------------------------------------------------
class ReportLightApi(ThreadedJsonGen):

    time_masks = ('%d.%m.%Y %H:%M', '%Y-%m-%d %H:%M:%S', '%Y-%m-%d', '%d.%m.%Y %H:%M:%S', '%d.%m.%Y')

    #-----------------------------------------------------------------------
    def _patch_sqlpar(self, sqlpar, rmd):
        i = 0
        for p in rmd.paramdefs:
            p2l = p[2].lower()
            if p2l == 'unixtime':
                if safe_int(sqlpar[i]) == 0:
                    t = 0
                    for mask in self.time_masks:
                        try:
                            t = int(time.mktime(time.strptime(sqlpar[i], mask)))
                            break
                        except ValueError:
                            pass

                    if t > 0: sqlpar[i] = t

            elif p2l == 'gdmdate':
                dt = None
                for mask in self.time_masks:
                    try:
                        dt = datetime.datetime(2016,1,1).strptime(sqlpar[i], mask)
                        break
                    except ValueError:
                        pass

                if dt: sqlpar[i] = dt.strftime('%Y%m%d')

            elif p2l == 'datetime3':
                dt = None
                for mask in self.time_masks:
                    try:
                        dt = datetime.datetime(2016,1,1).strptime(sqlpar[i], mask)
                        break
                    except ValueError:
                        pass

                if dt: sqlpar[i] = dt

            i += 1

        return sqlpar


    #-----------------------------------------------------------------------
    def _gen_work_dir(self, base_path):
        for i in range(20):
            dn = os.path.join(base_path, ''.join(random.choice(string.ascii_lowercase) for _ in range(8)))
            if os.path.exists(dn): continue
            os.mkdir(dn)
            return dn


    #-----------------------------------------------------------------------
    #
    # Param 'sqlres_json' in JSON format - hack for change fields "Type 'str' -> Type 'unicode'"
    #
    def _generate_Excel(self, rmd, sqlres_json, sqlsumres_json):
        # dname = self._gen_work_dir(REPORT_LIGHT_CACHE)
        # with open(dname + '/data.json', "wb") as f:
            # f.write(sqlres_json)

        sqlres = json.loads(sqlres_json)
        
        col_count = len(self.jQ['flds'])

        wb = xlwt.Workbook()
        ws = wb.add_sheet("Report")
        for i in range(col_count):
            ws.col(i).width = 20 * 256

        ## style0 = xlwt.easyxf('font: name Times New Roman, color-index red, bold on'
        ## style1 = xlwt.easyxf('font: name Calibri, height 220, bold on; align: wrap off, vert centre, horiz centre; borders: bottom thin')
        ws.write_merge(0, 0, 0, col_count-1, self.jQ['displayname'], xlwt.easyxf('font: name Calibri, height 360, bold on; align: wrap on, vert centre, horiz left'))

        ws.row(2).height = 600
        for i, f in enumerate(self.jQ['flds']):
            ws.write(2, i, f[0], xlwt.easyxf('font: height 220, bold on; align: wrap off, vert centre, horiz centre; borders: bottom thin'))

        sqlres_style = xlwt.easyxf('font: height 220, bold off; align: wrap on, vert centre, horiz left')
        for i, r in enumerate(sqlres):
            # for j, f in enumerate(r):
            for j, f in enumerate(rmd.fields):
                if f[1].startswith('sec2hms'):
                    ws.write(i+3, j, sec2hms(r[j]), sqlres_style)
                elif f[1].startswith('gdmdate'):
                    ws.write(i+3, j, gdmdate(r[j]), sqlres_style)
                else:
                    ws.write(i+3, j, r[j], sqlres_style)

        if sqlsumres_json:
            foot_style = xlwt.easyxf('font: height 220, bold on; align: wrap off, vert centre, horiz centre; borders: top thin')
            sqlsumres = json.loads(sqlsumres_json)[0]
            row_no = len(sqlres) + 3
            sum_no = 0
            for i, f in enumerate(rmd.fields):
                if f[2].startswith('sqlsum'):
                    try:
                        ws.write(row_no, i, sqlsumres[sum_no], foot_style)
                    except:
                        ws.write(row_no, i, 'Error', foot_style)
                    sum_no += 1
                else:
                    ws.write(row_no, i, '', foot_style)

        dname = self._gen_work_dir(REPORT_LIGHT_CACHE)
        fsn = self.jQ['lrid'] + '.xls'
        ffn = dname + '/' + fsn
        
        url = REPORT_LIGHT_CACHE_URL + os.path.basename(dname) + '/' + fsn
        wb.save(ffn)
        return url


    #-----------------------------------------------------------------------
    def runReport(self, fn):
        ## TODO: Check report_name path for attach like 'data/rep/../../../etc/hosts'
        rmd = LRMetadata(REPORT_LIGHT_REPOS + self.jQ['lrid'] + '.rprt')
        self.jQ['version'] = rmd.version
        self.jQ['displayname'] = rmd.displayname
        self.jQ['flds'] = rmd.fields
        
        sqlsumres = None

        lq = LightQueryFactory(rmd.getDbParams()).lq()
        if rmd.paramdefs:
            self.jQ['paramdefs'] = rmd.paramdefs
            if rmd.paramdicts: self.jQ['paramdicts'] = rmd.paramdicts
            if 'paramvals' in self.jQ:
                sqlpar = self._patch_sqlpar(self.jQ['paramvals'], rmd)
                print 'patch_sqlpar:', sqlpar
                sqlres = lq.get_rdata(rmd.sql, sqlpar)
                if rmd.sql_summary: sqlsumres = lq.get_rdata(rmd.sql_summary, sqlpar)
                self.jQ['lrstate'] = 4
            else:
                self.jQ['lrstate'] = 2
        else:
            sqlres = lq.get_rdata(rmd.sql)
            if rmd.sql_summary: sqlsumres = lq.get_rdata(rmd.sql_summary)
            self.jQ['lrstate'] = 4
            
        if self.jQ['lrstate'] == 4: 
            if fn == 'export-report':
                self.jQ['repurl'] = self._generate_Excel(rmd, json.dumps(sqlres, default=json_serialize_helper), json.dumps(sqlsumres, default=json_serialize_helper) if sqlsumres else None)
            else:
                self.jQ['rows'] = sqlres
                self.jQ['sums'] = sqlsumres

        return self.jQ

    
    #-----------------------------------------------------------------------
    def walk_in_thread(self):
        if self.request_method == "POST":
            self.template.Query = self.req.content.read()
            log.msg('json: ' + self.template.Query)
            jm = json.loads(self.template.Query)
            self.jQ = jm['q']
            fn = self.jQ['fn'].lower()

            if fn == 'run-report' or fn == 'export-report':
                self.template.Response = json.dumps({'r': self.runReport(fn)}, default=json_serialize_helper) ## , default=json_serialize_helper
            else:
                emsg = 'Unsupported function: ' + fn
                self.template.Response = json.dumps({'e': emsg})
                log.msg('WARNING: ' + emsg)

        else:
            self.template.Response = json.dumps({'e': "Unsupported request method."})


#---------------------------------------------------------------------------
resource = ReportLightApi(JsonTemplate())
