// LightReport internal library
// Author: Visokey 2015

var JsonRequest = Class.extend({
    init: function(url) {
        url = typeof url !== 'undefined' ? url : JsonRequest.webServiceUrl;
        this.async = true;
        this.request = {}
        this._url = url;
        //this._timeZoneOffset = (new Date()).getTimezoneOffset();
        this.error_handler = _.bind(this.error_handler, this);
        this.complete_handler = _.bind(this.complete_handler, this);
    },
    
    post: function(successHandler) {
        if (!JsonRequest.serviceUnavailable) {
            JsonRequest.spinner.show();
            $.ajax({
                async: this.async,
                type: 'POST',
                dataType: 'json',
                timeout: 5000,
                url: this._url,
                data: JSON.stringify({'q': this.request}),
                error: this.error_handler,
                complete: this.complete_handler,
                success: successHandler
            });
        }
    },

    error_alert: function(msg) {
        if (JsonRequest.errorAlertType === 2) {
            gsdb_error_dlg(msg);
        }
        else {
            bs_showerror(msg);
        }
    },
    
    error_handler: function(jqXHR, textStatus) {
        if (JsonRequest.serviceUnavailable) return;
        if (jqXHR.status === 0) {
            this.error_alert('Not connect. Verify Network.');
        } else if (jqXHR.status == 404) {
            this.error_alert('Requested page not found. [404]');
        } else if (jqXHR.status == 500) {
            this.error_alert('Internal Server Error [500].');
        } else if (jqXHR.status == 503) {
            JsonRequest.serviceUnavailable = true;
            this.error_alert('Service Temporarily Unavailable [503].');
        } else if (textStatus === 'parsererror') {
            this.error_alert('Requested JSON parse failed.');
        } else if (textStatus === 'timeout') {
            this.error_alert('Time out error.');
        } else if (textStatus === 'abort') {
            this.error_alert('Ajax request aborted.');
        } else {
            gsdb_error_dlg('Uncaught Error.\n' + jqXHR.responseText);
        }
    },

    complete_handler: function(jqXHR, textStatus) {
        JsonRequest.spinner.hide();
        var pwl = $("#pleasewait-lbl");
        if (pwl) {pwl.hide();}
        if(!this.async) {
            console.log(this);
        }
    }
},
// Static properties
{
    webServiceUrl: '/',
    spinner: false,
    serviceUnavailable: false,
    errorAlertType: 1
});

/*** ModalDialog functions ***/
function gsdb_error_dlg(msg) {
    var $divMsg = $('<div></div>');
    $divMsg.append(msg);
        
    BootstrapDialog.show({
        title: 'Error caused',
        message: $divMsg,
        buttons: [{
            label: 'OK',
            action: function(dialogRef) {
                dialogRef.close();
            }
        }, {
            label: 'Cancel',
            action: function(dialogRef) {
                dialogRef.close();
            }
        }]
    });
}

/**
  Function Name - bs_showalert()
  Inputs - message,alerttype
  Example - showalert("Invalid Login","alert-error")
  Types of alerts -- "alert-danger","alert-success","alert-info","alert-warning"
  Required - You only need to add a alert_placeholder div in your html page wherever you want to display these alerts "<div id="alert_placeholder"></div>"
**/

var _bs_showalert_closer = _.debounce(bs_hidealert, 30000);
function bs_showalert(message, alerttype) {
    var $div = $('#alert_div');
    if ($div.length > 0) {
        if (!$div.hasClass(alerttype)) {
            $div.removeClass();
            $div.addClass('alert ' + alerttype);
        }
        $div.children('span').text(message);
        _bs_showalert_closer();
    }
    else {
        $('#alert_placeholder').append('<div id="alert_div" class="alert '+alerttype+'"><a class="close" data-dismiss="alert">×</a><span>'+message+'</span></div>');
        _bs_showalert_closer();
    }
}

function bs_hidealert() {
    var $div = $('#alert_div');
    if ($div.length > 0) {
        $div.remove();
    }
}

function bs_showerror(message) {
    message = 'ERROR: ' + message;
    var $div = $('#error_div');
    if ($div.length > 0) {
        $div.children('span').text(message);
    }
    else {
        $('#alert_placeholder').append('<div id="error_div" class="alert alert-danger"><a class="close" data-dismiss="alert">×</a><span>'+message+'</span></div>');
    }
}

function getURLParameter(name) {
    return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search)||[,""])[1].replace(/\+/g, '%20'))||null;
}

/*** Format functions ***/
function fmtSec (sec) {
    var a = sec % 60;
    var b = (sec-a) / 60;
    var c = b % 60;
    var d = (b-c) / 60;
    a = a > 9 ? a : '0'+a;
    c = c > 9 ? c : '0'+c;
    d = d > 9 ? d : '0'+d;
    return d+':'+c+':'+a;
}

function toType(obj) {
  return ({}).toString.call(obj).match(/\s([a-zA-Z]+)/)[1].toLowerCase()
}
