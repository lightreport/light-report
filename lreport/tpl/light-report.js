// Light Report JS
var light_report_version = 1;
var light_report_paramdefs = null;

function lr_fieldtype_parse(ftype) {
//console.log('FT1:', ftype);
    var r1 = {};
    var r2 = [];
    var p = ftype.split('(');
    r1['datatype'] = $.trim(p[0]);
    r1['datasize'] = 0;
    r1['readonly'] = false;
    if (p.length > 1) {
        p = p[1].split(')')[0].split(';');
        for (var i=0;i<p.length;i++) {
            p[i] = $.trim(p[i]);
            if ((i == 0) && ($.isNumeric(p[i]))) {
                r1['datasize'] = parseInt(p[i]);
                r2.push(r1['datasize']);
            }
            else {
                var pi = $.trim(p[i]);
                r2.push(pi);
                if (pi == 'readonly') {
                    r1['readonly'] = true;
                }
            }
        }
    }
    r1['params'] = r2
//console.log('FT2:', r1);
    return r1;
}

var LineFormatter = Class.extend({
    init: function(flds) {
        var fmtFuncNames = {
            'sec2hms': 1,
            'url': 2,
            'gdmdate': 3
        };
        
        this.fmtFuncIds = [];
        this.fmtFuncParams = [];
        for (var i = 0; i < flds.length; i++) {
            var fmtFuncId = 0;
            var fmtFuncParam = [];
            var ft = lr_fieldtype_parse(flds[i][1]);
            if (ft.datatype in fmtFuncNames) {
                fmtFuncId = fmtFuncNames[ft.datatype];
                fmtFuncParam = ft.params;
            }    
            this.fmtFuncIds.push(fmtFuncId);
            this.fmtFuncParams.push(fmtFuncParam);
        }
    },
    
    formatRow: function(row) {
        var td = '';
        var res = '';
        for (var j = 0; j < row.length; j++) {
            if (row[j] === null) {
                td = '&nbsp;';
            }
            else {
                switch(this.fmtFuncIds[j]) {
                    case 0:
                        td = row[j];
                    break;

                    case 1:
                        td = fmtSec(row[j]);
                    break;

                    case 2:
                        if (this.fmtFuncParams[j].length > 0) {
                            var base_url = this.fmtFuncParams[j][0];
                            td = this._cr_url(base_url, row[j]);
                        }
                        else {
                            td = '&nbsp;';
                        }
                    break;
                    
                    case 3:
                        var s = row[j];
                        var y = s.substring(0,4);
                        var m = s.substring(4,6);
                        var d = s.substring(6,8);
                        td = d + '.' + m + '.' + y;
                    break;

                    default:
                        console.log('Bad format function');
                }
            }
            res += '<td>' + td + '</td>';
        }
        return res;
    },
    
    _cr_url: function(base_url, fname) {
        var res = '<a href="' + base_url + fname + '" target="_blank">link</a>';
        return res;
    }
});
    
function performAbout() {
    var $textAndPic = $('<div></div>');
    $textAndPic.append('Who\'s this? <br />');
    $textAndPic.append('<i><b>Light Report 0.1</b></i>');
    //$textAndPic.append('<img src="./images/pig.ico" />');
        
    BootstrapDialog.show({
            title: 'About',
            message: $textAndPic,
            buttons: [{
                label: 'OK',
                action: function(dialogRef){
                    dialogRef.close();
                }
            }, {
                label: 'Cancel',
                action: function(dialogRef){
                    dialogRef.close();
                }
            }]
        });
}

function showUserInfo(url) {
    var $text = $('<div></div>');
    var un = $('#userInfoBtn').text();
    $text.append('User name: ' + un + '<br/>');
        
    BootstrapDialog.show({
            title: 'User Info',
            message: $text,
            buttons: [{
                label: 'Close',
                action: function(dialogRef){
                    dialogRef.close();
                }
            }]
        });
}

function showExportUrl(url) {
    var $text = $('<div></div>');
    $text.append('You report successfully created<br/>');
    $text.append('<a href="' + url + '" target="_blank">Click this link for download it</a>');
        
    BootstrapDialog.show({
            title: 'Export',
            message: $text,
            buttons: [{
                label: 'Close',
                action: function(dialogRef){
                    dialogRef.close();
                }
            }]
        });
}

function formatDate(date) {
    var dd = date.getDate();
    if (dd < 10) dd = '0' + dd;

    var mm = date.getMonth() + 1;
    if (mm < 10) mm = '0' + mm;

    var yy = date.getFullYear();

    return dd + '.' + mm + '.' + yy;
}

function formatDisplayName(stemplate, paramvals) {
    var ret = stemplate;
    for (var i = 0; i < paramvals.length; i++) {
        ret = ret.replace('@{'+ i +'}', paramvals[i]);
    }
    if ((ret.charAt(0) == '<') && (ret.charAt(ret.length-1) == '>')) {
        return ret;
    }
    else {
        return '<h2>' + ret + '</h2>';
    }
}

function create_lr_table(displayname, flds) {
    var mc = $('#main-container')
    mc.empty();
    mc.append($('<div class="page-header">').append($(displayname)));
    var d2 = $('<div class="row">')
    var d3 = $('<div class="col-md-12">');
    var tbl = $('<table class="table table-striped" name="lr_table" id="lr_table">');
    var thead = $('<thead>');
    for (var i = 0; i < flds.length; i++) {
        thead.append($('<th>').text(flds[i][0]));
    }
    /*var tfoot = $('<tfoot>');
    for (var i = 0; i < flds.length; i++) {
        tfoot.append($('<th>').text('sum'));
    }*/
    tbl.append(thead);
    // tbl.append(tfoot);
    tbl.append('<tr><td colspan="'+ flds.length +'">[Empty resultset]</td></tr>');
    d3.append(tbl);
    d2.append(d3);
    mc.append(d2);
}

function fill_lr_table(flds, rows) {
    var res = '';
    var formatr = new LineFormatter(flds);
    for (var i = 0; i < rows.length; i++) {
        res += '<tr>' + formatr.formatRow(rows[i]) + '</tr>';
    }
    $('#lr_table tbody').html(res);
}

function fill_lr_table_sums(flds, sums) {
    var j = 0;
    var tfoot = $('<tfoot>');
    for (var i = 0; i < flds.length; i++) {
        if (flds[i][2].startsWith('sqlsum')) {
            tfoot.append($('<th>').text(sums[0][j++]));
        }
        else {
            tfoot.append($('<th>').text(' '));
        }
    }    
    $('#lr_table').append(tfoot);
}

function runReport(lrfn, lrid, lrstate, lrparams) {
    var jr = new JsonRequest();
    jr.request.fn = lrfn;
    jr.request.lrid = lrid;
    jr.request.lrstate = lrstate;
    if (lrstate == 3) {
        jr.request.paramvals = lrparams;
    }

    jr.post(function(data) {
        if ('r' in data) {
            if ('version' in data.r) {
                light_report_version = data.r.version;
                console.log('version>', light_report_version);
            }
            if (data.r.lrstate == 2) {
                if ('paramdefs' in data.r) {
                    light_report_paramdefs = data.r.paramdefs;
                    if ('paramdicts' in data.r) {
                        showParamDlg(data.r.paramdefs, data.r.paramdicts);
                    }
                    else {
                        showParamDlg(data.r.paramdefs, null);
                    }
                }
            }
            if (data.r.lrstate == 4) {
                if ('repurl' in data.r) {
                    showExportUrl(data.r.repurl);
                }
                else {
                    var dn = formatDisplayName(data.r.displayname, data.r.paramvals);
                    create_lr_table(dn, data.r.flds);
                    fill_lr_table(data.r.flds, data.r.rows);
                    if ('sums' in data.r) {
                        fill_lr_table_sums(data.r.flds, data.r.sums);
                    }
                }
            }
        }
        
        if ('m' in data) gsdb_error_dlg(data.m);
        if ('e' in data) gsdb_error_dlg(data.e);
    });
}

function myReportClick() {
    var lrid = $(this).data('lrid');
    $('#lreport_id').val(lrid);
    bs_hidealert();
    runReport('run-report', lrid, 1);

    // TODO: Remove hardcode
    //if (lrid == 'Voice_Callback_Monitor') {
    //    setTimeout(performRefreshAuto, 60000);
    //}
}

function performRefreshAuto() {
    var $lr_id = $('#lreport_id').val();

    // TODO: Remove hardcode
    //if ($lr_id == 'Voice_Callback_Monitor') {
    //    runReport('run-report', $lr_id);
    //    setTimeout(performRefreshAuto, 60000);
    //}
}

function performRefresh() {
    var lrid = $('#lreport_id').val();
    if (lrid != '') {
        var paramvals = load_from_stor('LReport_' + lrid, light_report_version);
        if (paramvals != '') {
            runReport('run-report', lrid, 3, paramvals);
        }
        else {
            runReport('run-report', lrid, 1);
        }
    }
    else {
        bs_showalert("WARNING: You have not selected report and can't use 'Refresh'.", 'alert-warning');
    }
}

function performExport() {
    var lrid = $('#lreport_id').val();
    if (lrid != '') {
        var paramvals = load_from_stor('LReport_' + lrid, light_report_version);
        if (paramvals != '') {
            runReport('export-report', lrid, 3, paramvals);
        }
        else {
            runReport('export-report', lrid, 1);
        }
    }
    else {
        bs_showalert("WARNING: You have not selected report and can't use 'Export'.", 'alert-warning');
    }
}

function createSelectCtrl(ctrlId, inpVal, paramDict) {
    var sel = $('<select class="form-control" id="'+ ctrlId +'" />');
    if (paramDict) {
        for (var i in paramDict) {
            if (inpVal == paramDict[i][0]) {
                $('<option>', {value: paramDict[i][0], text: paramDict[i][1]}).attr('selected', '1').appendTo(sel);
            }
            else {    
                $('<option>', {value: paramDict[i][0], text: paramDict[i][1]}).appendTo(sel);
            }
        }
    }
    return sel;
}

function createDatetimeCtrl(inputId, dateVal, disabledTime) {
    var divId = inputId + '_div';
    var div = $('<div class="input-group date" id="'+ divId +'">');
    var inp = $('<input type="text" class="form-control" id="'+ inputId +'" value="'+ dateVal +'"/>');
    var span = $('<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"/></span>');
    div.append(inp);
    div.append(span);
    if (disabledTime) {
        div.datetimepicker({locale: 'ru', format: 'DD.MM.YYYY'});
    }
    else {
        div.datetimepicker({locale: 'ru'});
    }
    return div;
}

function showParamDlg(paramdefs, paramdicts) {
    var form = $('#myform3 form');
    form.empty();
    
    var lrid = $('#lreport_id').val();
    var paramvals = load_from_stor('LReport_' + lrid, light_report_version);
    
    var d2 = $('<div class="form-group">');
    for (var i = 0; i < paramdefs.length; i++) {
        var parid = 'parid-' + paramdefs[i][0];
        var lbl = $('<label for="'+ parid +'" class="control-label">');
        lbl.text(paramdefs[i][1]);
        
        var inp = '';
        var inpVal = paramvals[i];
        var pd = lr_fieldtype_parse(paramdefs[i][2].toLowerCase());
        switch(pd.datatype) {
            case 'datetime3':
            case 'datetime':
            case 'unixtime':
                if (!inpVal) {
                    inpVal = formatDate(new Date())  +' 00:00:00';
                }
                inp = createDatetimeCtrl(parid, inpVal, false);
            break
            
            case 'datetime2':
            case 'unixtime2':
                if (!inpVal) {
                    inpVal = formatDate(new Date())  +' 23:59:59';
                }
                inp = createDatetimeCtrl(parid, inpVal, false);
            break

            case 'gdmdate':
                if (!inpVal) {
                    inpVal = formatDate(new Date())  +' 23:59:59';
                }
                inp = createDatetimeCtrl(parid, inpVal, true);
            break
            
            case 'dict':
                inp = createSelectCtrl(parid, inpVal, paramdicts[pd.params[0]]);
            break

            default:
                inp = $('<input type="text" class="form-control" id="'+ parid +'" value="'+ inpVal +'"/>');
        }

        d2.append(lbl);
        d2.append(inp);
    }
    form.append(d2);
    $('#myform3').modal('show');
}

function clickParamSet() {
    $('#myform3').modal('hide');
    var lrid = $('#lreport_id').val();
    var paramvals = [];
    $("#myform3 input,select").each(function(index, elem) {
        paramvals.push($(elem).val());
    });
    console.log('paramvals => ', paramvals);
    save_to_stor('LReport_' + lrid, light_report_version, paramvals);
    runReport('run-report', lrid, 3, paramvals);
}

function clickParamClose() {
    $('#lreport_id').val('');
}

function load_from_stor(key, version) {
    try {
        var s_val = localStorage.getItem(key);
        if (s_val != null) {
            s_val = JSON.parse(s_val);
            if ('val' in s_val) {
                if ('ver' in s_val) {
                    if (version != s_val['ver']) return '';
                }
                return s_val['val'];
            }
            else {
                return '';
            }
        }
        else {
            return '';
        }    
    } catch(e) {
        return '';
    }
}

function save_to_stor(key, version, value) {
    var s_val = JSON.stringify({'val': value, 'ver': version});
    try {
        localStorage.setItem(key, s_val);
        return true;
    } catch(e) {
        return false;
    }
}

function _init_js() {
    if (!String.prototype.startsWith) {
        String.prototype.startsWith = function(searchString, position) {
            position = position || 0;
            return this.substr(position, searchString.length) === searchString;
        };
    }
}

$(document).ready(function() {
    _init_js();

    $.ajaxSetup({scriptCharset: "utf-8"});
    JsonRequest.webServiceUrl = 'light-report-api';
    JsonRequest.spinner = $('#spinner');

    //$("#spinner").bind("ajaxSend", function() {
    //    $(this).show();
    //}).bind("ajaxStop", function() {
    //    $(this).hide();
    //}).bind("ajaxError", function() {
    //    $(this).hide();
    //});    


    $('#refreshBtn').click(_.throttle(performRefresh, 5000));
    $('#exportBtn').click(_.throttle(performExport, 5000));
    $('.myReportItem').click(myReportClick);
    
    $('#lrp-nav-options img').attr('src', user_profile_avatar);
});