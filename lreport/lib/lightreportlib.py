#!/usr/bin/env python
#---------------------------------------------------------------------------
# Name:        lightreportlib.py
# Purpose:     Light Report library
#
# Author:      Igor Lavrinenko
#
# Created:     2016
# RCS-ID:      $Id$
# Copyright:   (c) 2016
# License:     GPL
#---------------------------------------------------------------------------

import decimal, datetime, yaml
from myconfig import MyConfig
from connectpool import ConnectPoolParams
from twisted.python import log

#---------------------------------------------------------------------------
def json_serialize_helper(obj):
    if obj is None:
        return '(null)'
    else:
        if isinstance(obj, datetime.datetime):
            ret = obj.isoformat()
            return ret
        elif isinstance(obj, decimal.Decimal):
            ret = float(obj)
            return ret


#---------------------------------------------------------------------------
class LRMetadata(MyConfig):

    #-----------------------------------------------------------------------
    def check_options(self, config_file_opt, in_memory_opt):
        p3 = ConnectPoolParams('Oracle', 3)
        p3.params['host'] = '10.42.222.70'
        p3.params['port'] = 1521
        p3.params['sid'] = 'SID1'
        p3.params['username'] = 'voicemail'
        p3.params['password'] = 'password'
        in_memory_opt['dbPool_3'] = p3


    #-----------------------------------------------------------------------
    def getDbParams(self):
        pool_key = 'dbPool_' + str(getattr(self.config_file_opt, 'connectionid'))
        print 'pool_key =', pool_key
        return self.opt[pool_key]
