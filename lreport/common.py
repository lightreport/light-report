#!/usr/bin/env python
#---------------------------------------------------------------------------
# Name:        common.py
# Purpose:     Common functions and consts
#
# Author:      Igor Lavrinenko
#
# Created:     2016
# RCS-ID:      $Id$
# Copyright:   (c) 2016
# License:     GPL
#---------------------------------------------------------------------------

import string, os, sys

#---------------------------------------------------------------------------
WEBAPP_NAME      = 'Light Report Server'
WEBAPP_ROOT      = 'lreport'
#ifdef debug
_BASE_DIR        = 'n:/Projects/ccapps 2.0/lreport/'
_LIB_DIR         = 'n:/Projects/ccapps 2.0/lreport-common/'
#else
#define _BASE_DIR 'c:/Visokey/ccapps 2.0/lreport/'
#define _LIB_DIR 'c:/Visokey/ccapps 2.0/lreport-common/'
#endif
BASE_DIR         = _BASE_DIR
LIB_DIR          = _LIB_DIR

FORM_LIGHT_REPOS  = LIB_DIR + 'data/forms/'
REPORT_LIGHT_REPOS = LIB_DIR + 'data/rep/'

REPORT_LIGHT_CACHE = BASE_DIR + 'static/cache/rep/'
REPORT_LIGHT_CACHE_URL = 'cache/rep/'

LOG_DIR          = BASE_DIR + 'log/'
APP_TEMPLATE_DIR = BASE_DIR + 'tpl/'
INDEX_TEMPLATE   = APP_TEMPLATE_DIR + 'index.xml'
LOGIN_TEMPLATE   = APP_TEMPLATE_DIR + 'login.xml'
ERROR_PAGE_TEMPLATE = APP_TEMPLATE_DIR + 'errorpage.xml'

SERVER_INFO_TPL  = APP_TEMPLATE_DIR + 'server-info.xml'
FORM_LIGHT_TPL   = APP_TEMPLATE_DIR + 'light-form.xml'
REPORT_LIGHT_TPL = APP_TEMPLATE_DIR + 'light-report.xml'
REPORT_LIGHT_JST = APP_TEMPLATE_DIR + 'light-report.js'

CSS_BOOTSTRAP        = 'css/bootstrap.min.css'
CSS_BOOTSTRAP_DIALOG = 'css/bootstrap-dialog.min.css'
CSS_BOOTSTRAP_DATETIMEPICKER = 'css/bootstrap-datetimepicker.min.css'
CSS_STARTER          = 'css/starter-template.css'
JS_BOOTSTRAP         = 'js/bootstrap.min.js'
JS_BOOTSTRAP_DIALOG  = 'js/bootstrap-dialog.min.js'
JS_BOOTSTRAP_DATETIMEPICKER = 'js/bootstrap-datetimepicker.min.js'
JS_JQUERY            = 'js/jquery.min.js'
JS_UNDERSCORE        = 'js/underscore-min.js'
JS_MOMENTS           = 'js/moment-with-locales.min.js'
JS_CLASS_EXTEND      = 'js/class-extend.min.js'

#ifdef debug
_JS_LRTOOLS          = 'js/lrtools.js'
_PRINT_ERROR_TRACEBACK_TO_WEBUI = True
#else
#define _JS_LRTOOLS 'js/lrtools.min.js'
#define _PRINT_ERROR_TRACEBACK_TO_WEBUI False
#endif
JS_LRTOOLS           = _JS_LRTOOLS
PRINT_ERROR_TRACEBACK_TO_WEBUI = _PRINT_ERROR_TRACEBACK_TO_WEBUI

_SYS_PLATFORM = ''

#---------------------------------------------------------------------------
def safe_int(val, default_val=0):
    try:
        return int(val)
    except ValueError:
        return default_val


#---------------------------------------------------------------------------
def safe_float(val, default_val=0.0):
    try:
        return float(val)
    except ValueError:
        return default_val


#---------------------------------------------------------------------------
def getArgByName(args, name):
    res = ""
    for key, val in args:
        if key.lower() == name: res = string.join(val, ",") 
    return res


#---------------------------------------------------------------------------
def isWinPlatform():
    global _SYS_PLATFORM
    if _SYS_PLATFORM == '':
        if sys.platform.lower().startswith('win'): _SYS_PLATFORM = 'win'
        else: _SYS_PLATFORM = sys.platform
    return (_SYS_PLATFORM == 'win')


#---------------------------------------------------------------------------
def pathJoin(path_a, path_b):
    if isWinPlatform():
        return path_a + path_b if path_a[-1:] == '/' else path_a +'/'+ path_b
    else:    
        return os.path.join(path_a, path_b)


#---------------------------------------------------------------------------
def enum(**named_values):
    return type('Enum', (), named_values)


#---------------------------------------------------------------------------
class AttributeDict(dict):
    def __getattr__(self, key):
        return self[key]

    def __setattr__(self, key, value):
        self[key] = value

    def copy(self):
        return AttributeDict(self.iteritems())


#---------------------------------------------------------------------------
Vartainer = AttributeDict()
